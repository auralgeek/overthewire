objdumptoshellcode
==================

Handy bash function to get stripped hex code from an input binary.  Just drop
this into the bottom of your `~/.bashrc`.

```
function objdumptoshellcode () {
  for i in $(objdump -d $1 -M intel | grep "^ " | cut -f2); do echo -En '\x'$i; done; echo
}
```

Use like:

``` sh
$ objdumptoshellcode <some_executable_binary>
```
