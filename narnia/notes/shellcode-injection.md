Shellcode Injection
===================

1. Craft the shellcode as needed.
2. A point of entry is identified.
3. The program is exploited to transfer execution flow to the location of the
   shellcode insertion.

Crafting Shellcode
------------------

This step is handled in it's own files.

Point of Entry Identification
-----------------------------

To get an idea of what's going on in the guts, we do:

``` sh
$ objdump -d -M intel <target_binary>
```

In this we are looking for the address of the target buffer.  We might
typically expect to see relative addressing here, something like `$ebp - 0x6c`
perhaps, where `0x6c` is the size of the buffer.

Armed with that knowledge we can run the program with gdb and poke around:

``` sh
$ gdb -q <target_binary>
```

We can set a breakpoint somewhere in the code near the buffer we want to
attack, and run there.  Then we can print out the location of `$ebp` and/or
`$ebp - 0x6c` and expect this to be within a few bytes of the address at
runtime without gdb as long as ASLR is disabled.  This few bytes of error can
be handled by a `nop` sled.

Our payload for a 112 byte sized buffer immediately followed by the return
address of a function on stack.  An attack might be something like:

| 40 bytes of `nop` | 25 bytes of shellcode | 49 bytes random data | 4 bytes
pointing to the middle of the `nop` sled |

The final 4 bytes are to overwrite the return address, and the random data is
just to push that address the correct amount to overwrite the original return
address.  Then the function will "return" to the middle of our `nop` sled and
slide right into shellcode execution.  This obviously only works on a binary
with stack execution enabled, and no stack protection (to execute the shellcode
on stack, and overwrite the return address, respectively.)

The attack could look like this:

``` sh
$ <target_binary> $(python -c "print('\x90'*40 + '<shellcode>' + 'A'*47
+ '<address_of_middle_of_nops>')")
```

If this segfaults, you can try adjusting the address by +/- 40 bytes (the nop
sled size) a few times and try that.

Handling ALSR
-------------

One somewhat ghetto way of handling ALSR is to create a huge `nop` sled and
pick a randomish return address hoping it drops the execution flow somewhere in
that sled.  Environment variables are good candidates for holding our sled and
shellcode as they can be quite large in size, and are always loaded onto the
stack somewhere.  Set the variable, create the exploit payload which drops us
into stack randomly, and try execution over and over until you get your shell.

``` sh
$ export SHELLCODE=$(python -c "print('\x90'*100000 + '<shellcode>')")
$ $ for i in {1..100}; do <target_binary> $(python -c "print('A'*112
+ '<random_return_address_on_stack>')"); done
```
